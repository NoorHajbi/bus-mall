import { sleep } from 'k6';
import http from 'k6/http';

export const options = {
  duration: '1m',
  vus: 50, //virtual users
  thresholds: {
    http_req_duration: ['p(95)<500'], // 95 percent of response times must be below 500ms
    // Throughout this duration, each VU will generate one request, sleep for 3 seconds, and then start over.

  },
};


export default function () {
  http.get('https://noorhajbi.github.io/bus-mall/');
  sleep(3);
}

/** Note    
// Thresholds are a powerful feature providing a flexible API to define various types of pass/fail criteria in the same test run. For example:

// The 99th percentile response time must be below 700 ms.
// The 95th percentile response time must be below 400 ms.
// No more than 1% failed requests.
// The content of a response must be correct more than 95% of the time.
// Your condition for pass/fail criteria (SLOs)
**/
